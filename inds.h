#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include "darr.h"

#define ERROR_BUFLEN 256
enum inds_op_type {
	IOT_AND,
	IOT_OR,
	IOT_NOT,
};

$make_arr_with_member (int, inds, struct { int failed; char msg[256]; });
struct inds_info {
	void *arg, *obj;
	int idx;
	struct inds current_inds;
};
typedef bool (*inds_func) (struct inds_info info);

struct inds_event {
	char *keyword;
	void *arg;
	inds_func func;
	enum inds_op_type op_type;     // If func is 0, it's an op
};

struct inds get_inds (size_t n_objs, void *objs, size_t obj_size, size_t n_funcs,
		 inds_func *funcs, size_t n_events, struct inds_event *events);
