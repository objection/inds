#pragma once

/**
 * Copyright (c) 2014 rxi
 *
 * Fiddled with by me
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the MIT license. See LICENSE for details.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define $darr_n_arr(x) ((sizeof(x)/sizeof((x)[0])) / \
		((size_t)(!(sizeof(x) % sizeof((x)[0])))))

#define arr_unpack_(v) \
  (char**)&(v)->d, &(v)->n, &(v)->a, sizeof(*(v)->d)

// Since I've let you provide your own name, I'll continue to do that;
// that is, I won't name the struct something like <type>_arr_<size>.
// This means you'll have to name it something.
#define $make_static_arr(T, name, size) \
	struct name {T d[size]; size_t n; }
#define $make_arr(T, name) \
  struct name { T *d; size_t n, a; }
#define $make_arr_with_member(T, name, member) \
  struct name { T *d; size_t n, a; member; }

#define arr_init(v) \
  memset((v), 0, sizeof(*(v)))

#define arr_deinit(v) \
  ( free((v)->d),\
    arr_init(v) )

#define arr_push(v, val) \
  ({ arr_expand_(arr_unpack_(v)) \
   	? -1 \
	: ((v)->d[(v)->n++] = (val), (v)->n - 1); })

#define arr_add arr_push
#define arr_app arr_push

/* #define arr_push_m(v, vals, n) \ */
/*   ({ \ */
/* 	__typeof__ (v)->d *before = (v).d; \ */
/* 	arr_expand_(arr_unpack_(v)) ? -1 :\ */
/* 		((v)->d[(v)->n++] = (val), 0), 0; \ */
/* 		before; \ */
/*    }) */


#define arr_pop(v) \
  ({ (v)->d[--(v)->n]; })

#define arr_splice(v, start, count) \
  ( arr_splice_(arr_unpack_(v), start, count),\
    (v)->n -= (count) )

#define arr_swapsplice(v, start, count) \
  ( arr_swapsplice_(arr_unpack_(v), start, count),\
    (v)->n -= (count) )

#define arr_insert(v, _idx, val) \
	({ \
		size_t idx = _idx; \
		arr_insert_(arr_unpack_(v), idx) ? -1 :\
			((v)->d[idx] = (val), 0), (v)->n++, 0; \
	})

#define arr_insert_extending(v, _idx, val) \
	({ \
	 /* FIXME: do a better job that this hack. It's not that bad.*/ \
		if (_idx >= (v)->n) { \
			size_t old_a = (v)->n; \
			arr_reserve (v, _idx + 1); \
			memset ((v)->d + old_a, 0, sizeof *(v)->d * ((v)->a - old_a)); \
			(v)->d[_idx] = val; \
			(v)->n = _idx + 1; \
		} else \
			arr_insert (v, _idx, val); \
	})

// This seems fast enough. Actually, I haven't tested it at all.
// It returns a darr, so you can use it, hopefully, as a generic scratch allocator.
// You can go "struct str str = arr_push_m (&scratch, "hi there", 8)".
#define arr_insert_mul(v, idx, vals, n_insert)									\
	({ \
		/* You can't just use idx here, because if you've used, eg, arr.n,  */ \
		/* that will go up in array insert and cause an infinite loop */ \
		int _idx = idx; \
		for (int i = _idx; i < _idx + n_insert; i++) { \
			arr_insert (v, i, vals[i - _idx]); \
		} \
		(typeof (*(v))) {.d = (v)->d + idx, .n = n_insert, .a = -1}; \
	})

// It returns a darr, so you can use it, hopefully, as a generic scratch allocator.
// You can go "struct str str = arr_push_m (&scratch, "hi there", 8)".
#define arr_push_mul(v, vals, n_insert)									\
	({ \
		/* You can't just use idx here, because if you've used, eg, arr.n,  */ \
		/* that will go up in array insert and cause an infinite loop */ \
		arr_reserve (v, (n_insert)); \
		memcpy ((v)->d + (v)->n, (vals), (n_insert)); \
		(v)->n += n_insert; \
	})

// This pushes a real C array, hence you don't have to count the
// elements. It takes a "subtract" parameter. That's so you can
// optionally ignore the zero at the end of a string.
// Eg, you do 'arr_push_arr (&str, (char []) {"this"}, 1)' to
// not copy the zero after "this".
#define arr_push_arr(v, vals, subtract) \
	({ \
		int _idx = (*v).n; \
		int n_insert = $darr_n_arr (vals) - (subtract); \
		for (int i = _idx; i < _idx + n_insert; i++) { \
			arr_insert (v, i, vals[i - _idx]); \
		} \
		(typeof (*(v))) {.d = (v)->d + _idx, .n = n_insert, .a = -1}; \
	})

// See arr_push_arr.
#define arr_insert_arr(v, idx, vals, subtract)									\
	({ \
		int _idx = idx; \
		int n_insert = $darr_n_arr (vals) - subtract; \
		for (int i = _idx; i < _idx + n_insert; i++) { \
			arr_insert (v, i, vals[i - _idx]); \
		} \
		(typeof (*(v))) {.d = (v)->d + idx, .n = n_insert, .a = -1}; \
	})


#define arr_sort(v, fn) \
  qsort((v)->d, (v)->n, sizeof(*(v)->d), fn)

#define arr_swap(v, idx1, idx2) \
  arr_swap_(arr_unpack_(v), idx1, idx2)

#define arr_truncate(v, len) \
  ((v)->n = (len) < (v)->n ? (len) : (v)->n)

#define arr_clear(v) \
  ((v)->n = 0)

#define arr_first(v) \
  ({ &(v)->d[0]; })

#define arr_last(v) \
	({ &(v)->d[(v)->n - 1]; })

#define arr_reserve(v, n) \
  arr_reserve_(arr_unpack_(v), n)

#define arr_compact(v) \
  arr_compact_(arr_unpack_(v))

#define arr_pusharr(v, arr, count) \
  do { \
    int i__, n__ = (count); \
    if (arr_reserve_po2_(arr_unpack_(v), (v)->n + n__) != 0) break; \
    for (i__ = 0; i__ < n__; i__++) { \
      (v)->d[(v)->n++] = (arr)[i__]; \
    }\
  } while (0)

#define arr_pusharr_add_sentinal(v, arr, count, sentinal) \
	({ \
	 	arr_pusharr (v, arr, count); \
		arr_add (v, sentinal); \
	})

#define arr_extend(v, v2) \
  arr_pusharr((v), (v2)->d, (v2)->n)

#define arr_find(v, val, compar) \
	({ \
	 	typeof ((v)->d) res = 0; \
		for (int i = 0; i < (v)->n; i++) { \
			if (0 == ((compar) (&(v)->d[i], val))) { \
				res = (v)->d + i; \
				break; \
			} \
		} \
		res; \
	})


#define __find_by_member_r(arr, n_arr, val, compar, _member, arg) \
	({ \
	 	typeof (arr) res = 0; \
		size_t offset = offsetof (typeof (*(arr)), _member); \
		for (typeof (arr[0]) *elem = (arr); \
				elem < (arr) + (n_arr); elem++) { \
			char *base = (char *) elem; \
			/* Here's an interesting, tricky thing, this typeof, concatenated \* \
			 * the struct. It's the only way to make it work. Has to be a pointer */ \
			typeof ((arr)[0]._member) *p = (typeof ((arr)[0]._member) *) base + offset; \
			if (0 == ((compar) (val, *p, arg))) { \
				res = elem; \
				break; \
			} \
		} \
		res; \
	})
#define __find_by_member(arr, n_arr, val, compar, _member) \
	({ \
	 	typeof (arr) res = 0; \
		size_t offset = offsetof (typeof (*(arr)), _member); \
		for (typeof (arr[0]) *elem = (arr); \
				elem < (arr) + (n_arr); elem++) { \
			char *base = (char *) elem; \
			/* Here's an interesting, tricky thing, this typeof, concatenated \* \
			 * the struct. It's the only way to make it work. Has to be a pointer */ \
			typeof ((arr)[0]._member) *p = (typeof ((arr)[0]._member) *) base + offset; \
			if (0 == ((compar) (val, *p))) { \
				res = elem; \
				break; \
			} \
		} \
		res; \
	})
#define arr_find_by_member(v, val, compar, _member) \
	({ \
		__find_by_member ((v)->d, (v)->n, val, compar, _member); \
	})
#define arr_find_by_member_r(v, val, compar, _member, _arg) \
	({ \
		__find_by_member_r ((v)->d, (v)->n, val, compar, _member, _arg); \
	})

#define arr_simple_find(v, val) \
	({ \
	 	__typeof__ (*v) _*res = 0; \
		for ((idx) = 0; (idx) < (v)->n; (idx)++) { \
			if ((v)->d[(idx)] == (val)) { \
				res = &(v)->d[(idx)]; \
				break; \
			} \
		}\
		res; \
	})

#define arr_simple_remove(v, val) \
  do { \
    int idx__; \
    arr_simple_find(v, val, idx__); \
    if (idx__ != -1) arr_splice(v, idx__, 1); \
  } while (0)

#define arr_remove(v, val, compar, _free_code) \
  do { \
	  /* I tried using arr_find here, but clang complained. Said I was trying to cast */ \
	  /* an int to a pointer, which I consider a cheek. */ \
	  for (int i = 0; i < (v)->n; i++) { \
		  if (0 == (compar (&(v)->d[i], val))) { \
			if (_free_code && &(v)->d[i]) { \
			  _free_code (&(v)->d[i]); \
			} \
			arr_splice (v, i, 1); \
		  } \
	  } \
  } while (0)

#define arr_remove_r(v, val, compar, _free_code, user) \
  do { \
	  /* I tried using arr_find here, but clang complained. Said I was trying to cast */ \
	  /* an int to a pointer, which I consider a cheek. */ \
	  for (int i = 0; i < (v)->n; i++) { \
		  if (0 == (compar (&(v)->d[i], val, user))) { \
			if (_free_code && &(v)->d[i]) { \
			  _free_code ((v)->d[i]); \
			} \
			arr_splice (v, i, 1); \
		} \
	  } \
  } while (0)


// TODO: arr_find: remove all matching

#define arr_reverse(v) \
  do { \
    int i__ = (v)->n / 2; \
    while (i__--) { \
      arr_swap((v), i__, (v)->n - (i__ + 1)); \
    }\
  } while (0)


#define arr_each(v, elem_name) \
	for (typeof ((v)->d) elem_name = (v)->d; \
			elem_name < (v)->d + (v)->n; elem_name++)

#define arr_beach(v, elem_name) \
	for (typeof ((v)->d) elem_name = (v)->d + (v)->n; \
			elem_name >= (v)->d; elem_name--)

#define arr_insert_by_member_before(v, val, member_offset, member_type) \
	do { \
		char *val_member_offset = (char *) val + member_offset; \
		member_type *val_member = (member_type *)  val_member_offset; \
		printf ("val_member should be 4: %d\n", *val_member); \
		bool got = 0; \
		for (typeof ((v)->d) _p = (v)->d; _p < (v)->d + (v)->n; _p++) { \
			char *_p_offset = (char *) _p + member_offset; \
			member_type *v_member = (member_type *) _p_offset; \
			printf ("v_member[%zu] should be 6, 1, 9: %d\n", \
					_p - (v)->d, *v_member); \
			if (*val_member > *v_member) { \
				arr_insert (v, _p - (v)->d, *val); \
				got = 1; \
				break; \
			} \
		} \
		if (!got) \
			arr_insert (v, 0, *val); \
	} while (0)

#define arr_insert_by_member_after(v, val, member_offset, member_type) \
	do { \
		char *val_member_offset = (char *) val + member_offset; \
		member_type *val_member = (member_type *)  val_member_offset; \
		printf ("val_member should be 4: %d\n", *val_member); \
		bool got = 0; \
		for (typeof ((v)->d) _p = (v)->d; _p < (v)->d + (v)->n; _p++) { \
			char *_p_offset = (char *) _p + member_offset; \
			member_type *v_member = (member_type *) _p_offset; \
			printf ("v_member[%zu] should be 6, 1, 9: %d\n", \
					_p - (v)->d, *v_member); \
			if (*val_member >= *v_member) { \
				arr_insert (v, (_p - (v)->d) + 1, *val); \
				got = 1; \
				break; \
			} \
		} \
		if (!got) \
			arr_insert (v, 0, *val); \
	} while (0)


int arr_expand_ (char **d, size_t *n, size_t *a, int memsz);
int arr_reserve_ (char **d, size_t *n, size_t *a, int memsz, int n_reserve);
int arr_reserve_po2_ (char **d, size_t *n, size_t *a, int memsz, int n_reserve);
int arr_compact_ (char **d, size_t *n, size_t *a, int memsz);
int arr_insert_ (char **d, size_t *n, size_t *a, int memsz, int idx);
/* int arr_insert_multiple_(char **d, size_t *n, size_t *a, int memsz, */
/*                 int idx, int n_insert); */
void arr_splice_ (char **d, size_t *n, size_t *a, int memsz,
                 int start, int count);
void arr_swapsplice_ (char **d, size_t *n, size_t *a, int memsz,
                     int start, int count);
void arr_swap_ (char **d, size_t *n, size_t *a, int memsz,
               int idx1, int idx2);


/* typedef $make_arr(void*) arr_void_t; */
/* typedef $make_arr(char*) arr_str_t; */
/* typedef $make_arr(int) arr_int_t; */
/* typedef $make_arr(char) arr_char_t; */
/* typedef $make_arr(float) arr_float_t; */
/* typedef $make_arr(double) arr_double_t; */

