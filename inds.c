#define _GNU_SOURCE
#include "inds.h"
#include <assert.h>

#define $fori(_idx, _max) 																\
	for (typeof (_max + 0) _idx = 0; _idx < _max; _idx++)
#define $each(_item, _arr, _n) \
	for (__typeof__ (_arr[0]) *_item = _arr; _item < _arr + _n; _item++)
#define $free_if(_obj) \
	({ if (_obj) free (_obj); })

int cmp_ints (const void *a, const void *b) {
	return *(int *) a - *(int *) b;
}

int *remove_int_dups_inline (int **r, size_t *n_res) {

	// obviously no need to do anything it it's 0 or 1 long
	if (*n_res == 0 || *n_res == 1) return *r;

	int saved_res[*n_res];
	int nsaved_res = *n_res;

	memcpy (saved_res, *r, nsaved_res * sizeof *saved_res);
	qsort (saved_res, *n_res, sizeof *saved_res, cmp_ints);

	*n_res = 0;
	int *q = *r;

	int this = *saved_res;
	*q++ = this;
	(*n_res)++;
	for (int *p = saved_res; p - saved_res < nsaved_res; p++) {
		if (*p != this) {
			this = *p;
			*q++ = this;
			(*n_res)++;
		}
	}
	assert (*n_res);
	return *r;
}

void and_list (struct inds *pr, int *list2, size_t nlist2, bool rmdups) {

	struct inds tmp = {};
	arr_reserve (&tmp, pr->n);

	for (size_t i = 0; i < pr->n; ++i) {
		bool seen = 0;
		for (size_t j = 0; j < nlist2; ++j) {
			if (list2[j] == pr->d[i]) {
				seen = 1;
				break;
			}
		}
		if (seen)
			arr_add (&tmp, pr->d[i]);
	}

	if (pr->d)
		free (pr->d);
	*pr = tmp;

	if (rmdups)
		remove_int_dups_inline (&pr->d, &pr->n);
}

void or_list (struct inds *pr, int *list2, size_t nlist2, bool rmdups) {
	arr_insert_mul (pr, pr->n, list2, nlist2);

	if (rmdups)
		remove_int_dups_inline (&pr->d, &pr->n);
}

int not_list (struct inds *buf, int *b, size_t nb) {

	struct inds tmp = {};

	$fori (i, nb) {
		int match = 0;
		$fori (j, buf->n) {
			if (buf->d[j] == b[i])
				match = 1; //if flag=1 the elemnt is in A and in B
		}
		if (!match)
			arr_push (&tmp, b[i]);
	}
	free (buf->d);
	*buf = tmp;
	return 0;
}

static int error (char *error_buf, int event, char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);
	int len = 0;
	len += snprintf (error_buf, 256, "event %d: ", event);
	vsnprintf (error_buf + len, 256, fmt, ap);
	va_end (ap);
	return 0;
}

struct inds get_inds (size_t n_objs, void *objs, size_t obj_size, size_t n_funcs,
		inds_func *funcs, size_t n_events, struct inds_event *events) {

	struct inds r = {}, this_list = {};
	$fori (i, n_objs) arr_add (&r, i);

#define $error(fmt, ...) ({ \
		error (r.msg, i, fmt, ##__VA_ARGS__); \
		r.failed = 1; \
		goto out; \
	})

	enum inds_op_type op_type = 0;
	$fori (i, n_events) {
		this_list.n = 0;
		bool is_not = 0;
		struct inds_event *event = &events[i];
		assert (event->keyword);

		if (event->func)
			op_type = IOT_AND;
		else {
			if (event->op_type == IOT_NOT) {
				is_not = 1;
				op_type = IOT_AND;
			} else
				op_type = event->op_type;

			i++;
			if (i == n_events)
				$error ("A list needs to follow an operator (%s)", event->keyword);
			event = &events[i];
		}

		if (!event->func) {
			if (event->op_type == IOT_NOT) {
				is_not = 1;

				i++;
				if (i == n_events)
					$error ("You can't end with %s", event->keyword);
				event = &events[i];
			} else
				$error ("A list must be here");
		}

		$fori (j, n_objs) {

			void *obj = objs + (j * obj_size);

			bool rt = event->func (((struct inds_info)
					{.arg = event->arg, .obj = obj, .idx = j, .current_inds = r}));

			if (rt) arr_add (&this_list, j);
		}

		if (is_not)
			not_list (&this_list, r.d, r.n);

		switch (op_type) {
			case IOT_AND: and_list (&r, this_list.d, this_list.n, 1); break;
			case IOT_OR:  or_list (&r, this_list.d, this_list.n, 1);  break;
			case IOT_NOT: assert (!"Impossible");                     break;
		}
	}

out:
	if (r.failed)
		r.n = 0;
	$free_if (this_list.d);
	return r;
}

